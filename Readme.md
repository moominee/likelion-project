## SNS project

---

### 필수과제

---

- 회원가입
- Swagger API
- AWS EC2에 배포
- Gitlab CI & Crontab CD
- 로그인 기능
- 포스트 작성, 수정, 삭제, 리스트 구현

### 아키텍처

---
![](img1.png)

위 Layered Architecture 구조에 맞게 작성

- Println 사용 추천
    - Controller와 Service와 Repository의 호출 여부 확인
    - 디버깅시 유용함
- 각 단에 데이터를 전달 받을시 DTO 형태로 전달한다.

### ERD

---

![](img2.png)

---

### 회원가입 및 로그인 구현

- 회원은 일반회원(USER)과 ADMIN 회원이 있다.
- **회원가입** - **POST** `/join`
    - 회원가입 성공 시 `"회원가입 성공"` 을 리턴한다.
- **로그인** - **POST** `/login`
    - Spring Security와 JWT를 사용하여 구현한다.
    - 로그인 성공 시  `token`  을 리턴한다.
        - {”jwt”:”eyJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6Imt5~~~”}
- 입력 폼과 리턴 모두 JSON 형식.

---

### 포스트 작성, 상세조회, 수정, 삭제, 목록

- 포스트 **상세** - **GET `/posts/{postsId}`**
    - 회원, 비회원 모두 볼 수 있다.
    - 글의 제목, 내용, 글쓴이, 작성날짜, 마지막 수정날짜가 표시된다.
- 회원만이 글 작성을 할 수 있다.
- 포스트 **등록** - **POST `/posts`**
- 포스트 **수정** - **PUT** `/posts/{id}`  & **삭제** - **DELETE `/posts/{id}`**
    - ADMIN회원이나 글을 작성한 일반회원이 글에 대한 수정과 삭제를 할 수 있다.
- 리스트 - **GET** `/posts`
    - 회원, 비회원 모두 볼 수 있다.
    - 제목, 글쓴이, 마지막 수정날짜가 표시된다.
    - 포스트를 클릭하면 포스트의 상세 내역을 볼 수 있다.
    - 목록 기능은 페이징 기능이 포함된다. **(Pageable 사용)**
        - 한 페이지당 default 피드 갯수는 20개이다.
        - 총 페이지 갯수가 표시된다.
        - 작성날짜 기준으로 최신순으로 Sort한다.

---

### 미션 회고

- 미션을 부여받고 시작했으나, 아직 미션을 수행하기 어려운 수준임을 알게 됬다. 개인프로젝트를 어느정도 포기하더라도 스프링의 기초부터 다시 복습하고 어려운 부분은 강의를 통해 되짚어보기로 했다.
- 강사님의 미션 코드를 분석하면서 내가 잘 모르는 부분이 어떤 것인지, 더 확실하게 짚고 넘어가야할 부분이 어떤 것인지를 역방향으로 학습했다.
- 추상화에 많이 발목을 잡혔고, 특히 Security 관련 부분은 아직 코드를 보고도 이해가 가지 않는 부분이 많아 더 많은 시간을 투자해야 할 것 같다.