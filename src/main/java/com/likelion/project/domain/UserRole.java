package com.likelion.project.domain;

public enum UserRole {
    USER,
    ADMIN
}
