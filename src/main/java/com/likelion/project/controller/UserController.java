package com.likelion.project.controller;

import com.likelion.project.domain.dto.UserJoinRequest;
import com.likelion.project.domain.dto.UserLoginRequest;
import com.likelion.project.domain.entity.UserEntity;
import com.likelion.project.domain.response.Response;
import com.likelion.project.domain.response.UserJoinResponse;
import com.likelion.project.domain.response.UserLoginResponse;
import com.likelion.project.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/users")
public class UserController {

    private final UserService userService;

    @PostMapping("/join")
    public Response<UserJoinResponse> join(@RequestBody UserJoinRequest dto){
        UserEntity userEntity = userService.join(dto.getUserName(), dto.getPassword());
        UserJoinResponse userJoinResponse = UserJoinResponse.builder()
                .userId(userEntity.getId())
                .userName(userEntity.getUserName())
                .build();
        return Response.success(userJoinResponse);
    }

    @PostMapping("/login")
    public ResponseEntity<Response<UserLoginResponse>> log(@RequestBody UserLoginRequest dto){
        String token = userService.login(dto.getUserName(), dto.getPassword());
        return ResponseEntity.ok().body(Response.success(new UserLoginResponse(token)));
    }
}