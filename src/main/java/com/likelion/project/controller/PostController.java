package com.likelion.project.controller;


import com.likelion.project.domain.dto.*;
import com.likelion.project.domain.entity.CommentEntity;
import com.likelion.project.domain.entity.PostEntity;
import com.likelion.project.domain.response.CommentResponse;
import com.likelion.project.domain.response.CommentSimpleResponse;
import com.likelion.project.domain.response.PostResponse;
import com.likelion.project.domain.response.Response;
import com.likelion.project.service.PostService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/posts")
public class PostController {

    private final PostService postService;

    @PostMapping
    public Response<PostResponse> posts(@RequestBody PostRequest dto, Authentication authentication){
        System.out.println("Controller Test Enter");
        PostDto postDto = postService.write(dto.getTitle(),dto.getBody(), authentication.getName());
        System.out.println("Controller Test");
        return Response.success(new PostResponse("포스트 등록 완료", postDto.getId()));
    }

    @GetMapping("/{postId}")
    public ResponseEntity<PostDto> findById(@PathVariable Integer postId) {
        PostDto postDto = postService.get(postId);
        return ResponseEntity.ok().body(postDto);
    }

    @GetMapping
    public Response<Page<PostDto>> getPostList(@PageableDefault(size = 20)
                                               @SortDefault (sort = "createdAt",direction = Sort.Direction.DESC) Pageable pageable) {
        Page<PostDto> postDtos = postService.getAllItems(pageable);
        return Response.success(postDtos);
    }

    @PutMapping("/{id}")
    public Response<PostResponse> modify(@PathVariable Integer id, @RequestBody ModifyRequest dto, Authentication authentication) {
        System.out.println("Modify Controller Tes1");

        PostEntity postEntity = postService.modify(authentication.getName(), id, dto.getTitle(), dto.getBody());
        System.out.println("Modify Controller Tes3");
        return Response.success(new PostResponse("포스트 수정 완료", postEntity.getId()));
    }

    @DeleteMapping("/{postId}")
    public Response<PostResponse> delete(@PathVariable Integer postId, Authentication authentication) {
        System.out.println("Delete Controller Tes1");

        postService.delete(authentication.getName(), postId);
        return Response.success(new PostResponse("포스트 삭제 완료", postId));
    }

    @GetMapping("/my")
    public Response<Page<PostDto>> my(Pageable pageable, Authentication authentication) {
        Page<PostDto> my = postService.my(authentication.getName(),pageable);
        return Response.success(my);
    }

    @PostMapping("/{id}/comments")
    public Response<CommentResponse> comment(@PathVariable Integer id, @RequestBody PostCommentRequest request, Authentication authentication) {
        CommentEntity commentEntity = postService.comment(id, authentication.getName(), request.getComment());
        CommentResponse commentResponse = CommentResponse.fromComment(commentEntity);
        return Response.success(commentResponse);
    }

    @GetMapping("/{id}/comments")
    public Response<Page<CommentResponse>> comment(@PathVariable Integer id, @PageableDefault(size = 10)
    @SortDefault (sort = "createdAt",direction = Sort.Direction.DESC) Pageable pageable, Authentication authentication) {
        Page<CommentResponse> commentResponses = postService.getComments(id, pageable)
                .map(commentEntity -> CommentResponse.fromComment(commentEntity) );

        return Response.success(commentResponses);
    }

    @PutMapping("/{postId}/comments/{id}")
    public Response<CommentResponse> modifyComment(@PathVariable Integer id, @RequestBody CommentModifyRequest dto, Authentication authentication) {
        CommentEntity commentEntity = postService.modifyComment(authentication.getName(), id, dto.getComment());
        return Response.success(CommentResponse.fromComment(commentEntity));
    }
}