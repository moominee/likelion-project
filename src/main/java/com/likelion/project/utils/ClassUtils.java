package com.likelion.project.utils;

public class ClassUtils {

    public static <T> T getSafeCastInstance(Object o, Class<T> clazz) {
        return clazz != null && clazz.isInstance(o) ? clazz.cast(o) : null;
    }

    // 테스트 용도
    public static <T> T getTest(Object o, Class<T> clazz) {
        return clazz.cast(o);
    }
}
