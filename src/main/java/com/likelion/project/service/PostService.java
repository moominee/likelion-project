package com.likelion.project.service;

import com.likelion.project.domain.dto.PostDto;
import com.likelion.project.domain.dto.PostRequest;
import com.likelion.project.domain.dto.post.PostDto;
import com.likelion.project.domain.dto.post.PostRequest;
import com.likelion.project.exception.ErrorCode;
import com.likelion.project.repository.PostRepository;
import com.likelion.project.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.apache.catalina.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class PostService {

    private final UserRepository userRepository;

    private final PostRepository postRepository;

    public PostDto create(PostRequest postRequest, String userName) {

        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new Exception(ErrorCode.INVALID_PERMISSION));

        Post savedPost = postRepository.save(Post.of(postRequest.getTitle(), postRequest.getBody(), user));

        PostDto postDto = PostDto.builder()
                .postId(savedPost.getPostId())
                .build();

        return postDto;
    }

    public Page<PostDto> readAll(Pageable pageable) {

        Page<Post> posts = postRepository.findAll(pageable);
        Page<PostDto> list = PostDto.toPostList(posts);
        return list;
    }

    public PostDto readById(Long id) {
        Post post = postRepository.findById(id)
                .orElseThrow(() -> new Exception(ErrorCode.POST_NOT_FOUND));
        PostDto postDto = PostDto.toPost(post);
        return postDto;
    }

    @Transactional
    public PostDto update(Long id, PostRequest postRequest, String userName) {

        Post post = postRepository.findById(id)
                .orElseThrow(() -> new Exception(ErrorCode.POST_NOT_FOUND);

        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new Exception(ErrorCode.USERNAME_NOT_FOUND));

        if (!Objects.equals(post.getUser().getUserName(), user.getUserName())) {
            throw new Exception(ErrorCode.INVALID_PERMISSION);
        }

        post.setTitle(postRequest.getTitle());
        post.setBody(postRequest.getBody());
        Post updatedPost = postRepository.saveAndFlush(post);

        PostDto postDto = PostDto.builder()
                .postId(updatedPost.getPostId())
                .build();

        return postDto;
    }
}