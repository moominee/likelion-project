package com.likelion.project.service;

import com.likelion.project.domain.response.UserJoinResponse;
import com.likelion.project.domain.entity.User;
import com.likelion.project.exception.ErrorCode;
import com.likelion.project.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder;

    @Value("${jwt.token.secret}")
    private String key;
    private final Long expireTimeMs = (long) (1000 * 60 * 60);

    public UserJoinResponse join(String userName, String password) {

        userRepository.findByUserName(userName)
                .ifPresent(user -> {
                    throw new Exception(ErrorCode.DUPLICATED_USER_NAME);
                });

        User user = User.builder()
                .userName(userName)
                .password(encoder.encode(password))
                .build();
        userRepository.save(user);

        return new UserJoinResponse(user.getUserId(), user.getUserName());
    }

    public String login(String userName, String password) {

        User selectedUser = userRepository.findByUserName(userName)
                .orElseThrow(() -> new Exception(ErrorCode.USERNAME_NOT_FOUND));

        if (!encoder.matches(password, selectedUser.getPassword())) {
            throw new Exception(ErrorCode.INVALID_PASSWORD);
        }

        String token = JwtTokenUtil.createToken(selectedUser.getUserName(), key, expireTimeMs);
        log.info("token : {}", token);
        return token;
    }
}